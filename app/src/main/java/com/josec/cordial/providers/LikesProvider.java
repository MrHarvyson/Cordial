package com.josec.cordial.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.josec.cordial.models.Like;

public class LikesProvider {

    CollectionReference collectionReference;

    public LikesProvider(){
        collectionReference = FirebaseFirestore.getInstance().collection("Likes");
    }

    public Task<Void> create(Like like){
        DocumentReference documentReference = collectionReference.document();
        String id = documentReference.getId();
        like.setId(id);
        return  documentReference.set(like);
    }

    // eliminar like
    public Task<Void> delete(String id){
        return collectionReference.document(id).delete();
    }

    // devuelve cantidad de like de la publicacion
    public Query getLikesByPost(String idPost){
        return collectionReference.whereEqualTo("idPost",idPost);
    }

    // consulta a la base de datos para averiguar si el like del usuario existe dentro de la publicación
    public Query getLikeByPostAndUser(String idPost, String idUser){
        return collectionReference.whereEqualTo("idPost",idPost).whereEqualTo("idUser",idUser);
    }

    // consulta para recuperar likes de todos los usuarios de un post
    public Query getLikeByPostAndUser(String idPost){
        return collectionReference.whereEqualTo("idPost",idPost);
    }

}
