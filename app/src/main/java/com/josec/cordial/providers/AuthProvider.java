/* Clase que sirve para autentificar el usuario, desde esta clase podemos acceder al id del usuario
y buscarlo en la base de datos */

package com.josec.cordial.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class AuthProvider {

    private FirebaseAuth authProvider;

    public AuthProvider() {
        authProvider = FirebaseAuth.getInstance();
    }

    // resultado de la autentificacion
    public Task<AuthResult> googleLogin(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        return authProvider.signInWithCredential(credential);
    }

    // devuelve el id del usuario logeado
    public String getUid() {
        if (authProvider.getCurrentUser() != null) {
            return authProvider.getCurrentUser().getUid();
        } else {
            return null;
        }
    }

    // devuelve el email del usuario logeado
    public String getEmail() {
        if (authProvider.getCurrentUser() != null) {
            return authProvider.getCurrentUser().getEmail();
        } else {
            return null;
        }
    }

}
