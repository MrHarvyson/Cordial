package com.josec.cordial.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.josec.cordial.models.User;

import java.util.HashMap;
import java.util.Map;

public class UserProvider {

    private CollectionReference collectionReference;

    public UserProvider(){
        collectionReference = FirebaseFirestore.getInstance().collection("User");
    }

    // trae valores usuario
    public Task<DocumentSnapshot> getUser(String idUser){
        return collectionReference.document(idUser).get();
    }

    // crea usuario en firebase
    public Task<Void> createUser(User user){
        return collectionReference.document(user.getId()).set(user);
    }

    // actualiza usuario en firebase
    public Task<Void> updateUser(User user){
        Map<String,Object> map = new HashMap<>();
        map.put("nick", user.getNick());
        map.put("nombre", user.getNombre());
        map.put("primerApellido", user.getPrimerApellido());
        map.put("segundoApellido", user.getSegundoApellido());
        map.put("foto", user.getFoto());
        map.put("imagenPerfil", user.getImagenPerfil());
        return collectionReference.document(user.getId()).update(map);
    }


}
