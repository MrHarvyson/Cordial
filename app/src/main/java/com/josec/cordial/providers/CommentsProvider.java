package com.josec.cordial.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.josec.cordial.models.Comment;

public class CommentsProvider {
    CollectionReference mCollection;
    public CommentsProvider(){
        mCollection = FirebaseFirestore.getInstance().collection("comments");

    }

    // crea comentario
    public Task<Void> create(Comment comment){
        DocumentReference documentReference = mCollection.document();
        String id = documentReference.getId();
        comment.setId(id);
        return documentReference.set(comment);
    }

    // borra comentario
    public Task<Void> remove(String idComment){
        return mCollection.document(idComment).delete();
    }

    // devuelve todos los comentarios de un post - crear en firebase multiple consulta
    public Query getCommentByUser(String idPost){
        return mCollection.whereEqualTo("idPost",idPost).orderBy("timeStamp", Query.Direction.DESCENDING);
    }

    // devuelve la cantidad de comentarios de un post
    public Query getCountCommentByPost(String idPost){
        return mCollection.whereEqualTo("idPost",idPost);
    }

}
