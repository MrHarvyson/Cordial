package com.josec.cordial.providers;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.josec.cordial.models.Post;

import java.util.Collection;

public class PostProvider {

    CollectionReference collectionReference;

    public PostProvider(){
        collectionReference = FirebaseFirestore.getInstance().collection("Post");
    }

    // guardar publicacion
    public Task<Void> save(Post post){
        DocumentReference documentReference = collectionReference.document();
        String id = documentReference.getId();
        post.setId(id);
        return documentReference.set(post);
    }

    //eliminar publicacion
    public Task<Void> delete(String idPost){
        return collectionReference.document(idPost).delete();
    }

    // consultar todas las publicaciones
    public Query getAll(){
        return collectionReference.orderBy("timeStamp",Query.Direction.DESCENDING);
    }

    // consultar por titulos las publicaciones - search
    public Query getPostByTitle(String title){
        return collectionReference.orderBy("titulo").startAt(title).endAt(title + '\uf8ff');
    }

    // traer valores de una publicacion por el id de la publicacion
    public Task<DocumentSnapshot> getPostById(String idPost){
        return collectionReference.document(idPost).get();
    }

}
