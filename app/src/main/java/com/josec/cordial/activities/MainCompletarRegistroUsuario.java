package com.josec.cordial.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.josec.cordial.R;
import com.josec.cordial.models.User;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.UserProvider;

import dmax.dialog.SpotsDialog;

public class MainCompletarRegistroUsuario extends AppCompatActivity {

    ImageView logo, google;
    TextView marca, version;
    EditText nombre, apellidoPrimero, apellidoSegundo, etNick;
    Button entrar;
    UserProvider mUserProvider;
    AuthProvider mAuthProvider;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_completar_registro_profesorado);

        //para ocultar barra con el titulo
        getSupportActionBar().hide();

        nombre = findViewById(R.id.txtNombre);
        apellidoPrimero = findViewById(R.id.txtPrimerApellido);
        apellidoSegundo = findViewById(R.id.txtSegundoApellido);
        entrar = findViewById(R.id.btnFinalizar);
        logo = findViewById(R.id.imgLogo);
        google = findViewById(R.id.imgProfesorado);
        marca = findViewById(R.id.txtTitulo);
        version = findViewById(R.id.txtVersion);
        etNick = findViewById(R.id.etNickCompletar);

        mUserProvider = new UserProvider();
        mAuthProvider = new AuthProvider();

        alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Cargando...")
                .setCancelable(false).build();

        entrar.setOnClickListener(v -> {
            alertDialog.show();
            if (!nombre.getText().toString().equals(" ") || !apellidoPrimero.getText().toString().equals(" ") || !apellidoSegundo.getText().toString().equals(" ")) {
                createUser(etNick.getText().toString(),nombre.getText().toString(), apellidoPrimero.getText().toString(), apellidoSegundo.getText().toString());
            } else {
                Toast.makeText(this, "Complete los campos.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createUser(String nick, String nombre, String apellidoPrimero, String apellidoSegundo) {
        User user = new User();
        user.setId(mAuthProvider.getUid());
        user.setEmail(mAuthProvider.getEmail());
        user.setNick(nick);
        user.setNombre(nombre);
        user.setPrimerApellido(apellidoPrimero);
        user.setSegundoApellido(apellidoSegundo);
        user.setRol("c");

        mUserProvider.createUser(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Intent intent = new Intent(MainCompletarRegistroUsuario.this, MainHome.class);
                    startActivity(intent);
                    alertDialog.dismiss();
                    finish();
                } else {
                    alertDialog.dismiss();
                    Toast.makeText(MainCompletarRegistroUsuario.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
