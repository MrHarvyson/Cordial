package com.josec.cordial.activities;

import android.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.josec.cordial.R;
import com.josec.cordial.databinding.ActivityMainHomeBinding;
import dmax.dialog.SpotsDialog;

public class MainHome extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    ActivityMainHomeBinding binding;
    AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //para ocultar barra con el titulo
        getSupportActionBar().hide();


        binding = ActivityMainHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        NavController navController = navHostFragment.getNavController();

        alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Cargando...")
                .setTheme(R.style.Custom)
                .setCancelable(false).build();

        bottomNavigationView = findViewById(R.id.barraNavegacion);

        binding.barraNavegacion.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navegation_home:
                    navController.navigate(R.id.homeFragment);
                    break;
                case R.id.navegation_chat:
                    navController.navigate(R.id.chatFragment);
                    break;
                case R.id.navegation_incidencias:
                    navController.navigate(R.id.incidenciaFragment);
                    break;
                case R.id.navegation_perfil:
                    navController.navigate(R.id.perfilFragment);
                    break;
            }

            return true;
        });


    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainHome.this);

        LayoutInflater inflater= getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_dialog_exit,null);

        builder.setView(view);
//cambiso

        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        Button si = view.findViewById(R.id.btnSi);
        Button no = view.findViewById(R.id.btnNo);

        si.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainLogin.cerraSesion();
                Intent intent = new Intent(MainHome.this, MainLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

}