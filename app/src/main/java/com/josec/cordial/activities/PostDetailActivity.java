package com.josec.cordial.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codesgood.views.JustifiedTextView;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.josec.cordial.R;
import com.josec.cordial.adapters.CommentAdapter;
import com.josec.cordial.adapters.SliderAdapter;
import com.josec.cordial.models.Comment;
import com.josec.cordial.models.Post;
import com.josec.cordial.models.SliderItem;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.CommentsProvider;
import com.josec.cordial.providers.PostProvider;
import com.josec.cordial.providers.UserProvider;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class PostDetailActivity extends AppCompatActivity {

    SliderView sliderView;
    SliderAdapter sliderAdapter;
    List<SliderItem> mSliderItems = new ArrayList<>();
    String idPost;
    PostProvider postProvider;
    UserProvider userProvider;
    TextView tvNick, tvTituloPublicacion,txtDatePublicDetails;
    JustifiedTextView tvDescripcionPublicacion;
    CircleImageView civFoto;
    ImageView btnAtras;
    AlertDialog alertDialog;
    FloatingActionButton btnMensaje;
    AuthProvider authProvider;
    CommentsProvider commentsProvider;
    RecyclerView recyclerView;
    CommentAdapter commentAdapter;
    TextView txtNoComments;
    Post post;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        //para ocultar barra con el titulo
        getSupportActionBar().hide();

        sliderView = findViewById(R.id.imageSlider);
        postProvider = new PostProvider();
        userProvider = new UserProvider();
        commentsProvider = new CommentsProvider();
        authProvider = new AuthProvider();
        post = new Post();

        idPost = getIntent().getStringExtra("idPost");
        tvNick = findViewById(R.id.nickPerfil);
        btnMensaje = findViewById(R.id.btnMensaje);
        civFoto = findViewById(R.id.imgFotoPubli);
        tvTituloPublicacion = findViewById(R.id.tituloPublicacion);
        tvDescripcionPublicacion = findViewById(R.id.tvDescripcionPublicacion);
        btnAtras = findViewById(R.id.btnAtras);
        recyclerView = findViewById(R.id.recycleViewCommet);
        txtNoComments = findViewById(R.id.txtNoComments);




        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PostDetailActivity.this){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        recyclerView.setLayoutManager(linearLayoutManager);
        alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Cargando...")
                .setTheme(R.style.Custom)
                .setCancelable(false).build();
        alertDialog.show();
        getPost();
        btnAtras.setOnClickListener(v -> {
            finish();
        });

        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(PostDetailActivity.this);

                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.alert_dialog_mensaje, null);

                builder.setView(view);


                final AlertDialog dialog = builder.create();
                dialog.show();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

                Button si = view.findViewById(R.id.btnSi);
                Button no = view.findViewById(R.id.btnNo);
                EditText introMensaje = view.findViewById(R.id.introMensaje);

                si.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String mensaje = introMensaje.getText().toString();
                        if (!mensaje.isEmpty()) {
                            Comment comment = new Comment();
                            comment.setComment(mensaje);
                            comment.setIdPost(idPost);
                            comment.setIdUser(authProvider.getUid());
                            comment.setTimeStamp(new Date().getTime());
                            commentsProvider.create(comment).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(PostDetailActivity.this, "Comentario creado correctamente", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(PostDetailActivity.this, "Fallo al crear comentario nuevo.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            dialog.dismiss();
                        } else {
                            Toast.makeText(PostDetailActivity.this, "Debe ingresar comentario", Toast.LENGTH_SHORT).show();
                        }
                        onStart();
                    }


                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Query query = commentsProvider.getCommentByUser(idPost);
        FirestoreRecyclerOptions<Comment> options = new FirestoreRecyclerOptions.Builder<Comment>().setQuery(query, Comment.class).build();
        commentAdapter = new CommentAdapter(options, PostDetailActivity.this);
        recyclerView.setAdapter(commentAdapter);
        commentAdapter.startListening();

        commentsProvider.getCountCommentByPost(idPost).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int count = queryDocumentSnapshots.size();
                if(count==0){
                    txtNoComments.setText("No hay comentarios");
                }
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        commentAdapter.stopListening();
    }

    private void ejecutar() {
        sliderAdapter = new SliderAdapter(PostDetailActivity.this, mSliderItems);
        sliderView.setSliderAdapter(sliderAdapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.THIN_WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setScrollTimeInSec(5);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();
    }

    private void getPost() {
        postProvider.getPostById(idPost).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                alertDialog.dismiss();
                if (documentSnapshot.exists()) {
                    if (documentSnapshot.contains("imagen1")) {
                        String image1 = documentSnapshot.getString("imagen1");
                        SliderItem item = new SliderItem();
                        item.setImageUrl(image1);
                        mSliderItems.add(item);
                    }
                    if (documentSnapshot.contains("imagen2")) {
                        String image2 = documentSnapshot.getString("imagen2");
                        SliderItem item = new SliderItem();
                        item.setImageUrl(image2);
                        mSliderItems.add(item);
                    }
                    if (documentSnapshot.contains("titulo")) {
                        String titulo = documentSnapshot.getString("titulo").toUpperCase();
                        tvTituloPublicacion.setText(titulo);
                    }
                    if (documentSnapshot.contains("descripcion")) {
                        String descripcion = documentSnapshot.getString("descripcion");
                        tvDescripcionPublicacion.setText(descripcion);

                    }
                    if (documentSnapshot.contains("idUsuario")) {
                        String idUsuario = documentSnapshot.getString("idUsuario");
                        userProvider.getUser(idUsuario).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if (documentSnapshot.exists()) {
                                    if (documentSnapshot.contains("nick")) {
                                        String nick = documentSnapshot.getString("nick");
                                        tvNick.setText(nick);
                                    }
                                    if (documentSnapshot.contains("foto")) {
                                        String foto = documentSnapshot.getString("foto");
                                        Picasso.get().load(foto).into(civFoto);
                                    }
                                }
                            }
                        });
                    }

                    ejecutar();
                }
            }
        });
    }


}