package com.josec.cordial.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.storage.UploadTask;
import com.josec.cordial.R;
import com.josec.cordial.models.Post;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.ImageProvider;
import com.josec.cordial.providers.PostProvider;
import com.josec.cordial.utils.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import dmax.dialog.SpotsDialog;

public class PostActivity extends AppCompatActivity {

    CardView btnImg1, btnImg2;
    ImageView imgImagen1, imgImagen2;
    Button btnPublicar;
    boolean bImg1 = false;
    boolean bImg2 = false;
    ImageProvider imageProvider;
    File mImageFile1, mImageFile2;
    TextInputEditText inputTitulo, inputDescripcion;
    PostProvider postProvider;
    String txtTitulo, txtDescripcion = "";
    AuthProvider authProvider;
    AlertDialog alertDialog;
    private static final String TAG = "logeron";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        //para ocultar barra con el titulo
        getSupportActionBar().hide();

        imageProvider = new ImageProvider();
        postProvider = new PostProvider();
        authProvider = new AuthProvider();

        btnPublicar = findViewById(R.id.btnPublicar);
        btnImg1 = findViewById(R.id.cardImg1Publicacion);
        btnImg2 = findViewById(R.id.cardImg2Publicacion);
        imgImagen1 = findViewById(R.id.img1Publicacion);
        imgImagen2 = findViewById(R.id.img2Publicacion);
        inputTitulo = findViewById(R.id.inputTituloPuplicacion);
        inputDescripcion = findViewById(R.id.inputDescripcionPublicacion);
        alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Publicando...")
                .setTheme(R.style.Custom)
                .setCancelable(false).build();

        btnImg1.setOnClickListener(v -> {

            ImagePicker.Companion.with(PostActivity.this)
                    .cropSquare()
                    .compress(1024)
                    .maxResultSize(1080, 1080)
                    .start(101);
            bImg1 = true;

        });

        btnImg2.setOnClickListener(v -> {

            ImagePicker.Companion.with(PostActivity.this)
                    .cropSquare()
                    .compress(1024)
                    .maxResultSize(1080, 1080)
                    .start(101);
            bImg2 = true;
        });

        btnPublicar.setOnClickListener(v -> {

            if (inputTitulo.getText().toString().equals(" ") || inputDescripcion.getText().toString().equals(" ") || mImageFile1 == null || mImageFile2 == null) {
                Toast.makeText(this, "Faltan datos", Toast.LENGTH_SHORT).show();
            } else {
                alertDialog.show();
                clickPost();
                Intent intent = new Intent(PostActivity.this, MainHome.class);
                startActivity(intent);
            }
        });
    }

    private void clickPost() {
        txtTitulo = this.inputTitulo.getText().toString().trim();
        txtDescripcion = this.inputDescripcion.getText().toString().trim();
        guardarPublicacion();
    }

    private void guardarPublicacion() {

        imageProvider.save(PostActivity.this, mImageFile1).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskImagen1) {
                alertDialog.dismiss();
                if (taskImagen1.isSuccessful()) {
                    imageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri1) {
                            String url1 = uri1.toString();

                            imageProvider.save(PostActivity.this, mImageFile2).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskImagen2) {
                                    if (taskImagen2.isSuccessful()) {
                                        imageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri2) {
                                                String url2 = uri2.toString();
                                                Post post = new Post();
                                                post.setImagen1(url1);
                                                post.setImagen2(url2);
                                                post.setTitulo(txtTitulo);
                                                post.setDescripcion(txtDescripcion);
                                                post.setIdUsuario(authProvider.getUid());
                                                post.setTimeStamp(new Date().getTime());
                                                postProvider.save(post).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> taskSave) {
                                                        if (taskSave.isSuccessful()) {
                                                            Toast.makeText(PostActivity.this, "Información almacenada correctamente", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Toast.makeText(PostActivity.this, "Problemas al almacenar la informacion", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });

                        }
                    });
                    Toast.makeText(PostActivity.this, "Se almacenó correctamente la imagen", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "imagen1 almacenada");

                } else {
                    Toast.makeText(PostActivity.this, "No se pudo almacenar la imagen", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "error al almacenar imagen2");
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && bImg1) {
            Uri path = data.getData();
            imgImagen1.setImageURI(path);
            bImg1 = false;
            bImg2 = false;
            try {
                mImageFile1 = FileUtil.from(this, path);
            } catch (IOException e) {
                Log.d(TAG, "Error al pasar url a file");
            }
        }
        if (resultCode == Activity.RESULT_OK && bImg2) {
            Uri path = data.getData();
            imgImagen2.setImageURI(path);
            bImg1 = false;
            bImg2 = false;
            try {
                mImageFile2 = FileUtil.from(this, path);
            } catch (IOException e) {
                Log.d(TAG, "Error al pasar url a file");
            }
        }
    }
}