package com.josec.cordial.activities;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.josec.cordial.utils.Animacion;
import com.josec.cordial.R;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.UserProvider;
import dmax.dialog.SpotsDialog;

public class MainLogin extends AppCompatActivity {

    ImageView imgLogo, btnAlumnado, btnProfesorado;
    TextView txtTitulo, txtVersion, txtAlumnado, txtProfesorado;
    public static String id;
    private static final String TAG = "MainLogin";
    private static final int RC_SIGN_IN = 9001;
    public static GoogleSignInClient mGoogleSignInClient;
    AuthProvider authProvider;
    UserProvider userProvider;
    GoogleSignInOptions googleSignInOptions;
    boolean esProfesorado = false;
    AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //para ocultar barra con el titulo
        getSupportActionBar().hide();

        imgLogo = findViewById(R.id.imgLogo);
        txtTitulo = findViewById(R.id.txtTitulo);
        btnAlumnado = findViewById(R.id.imgAlumnado);
        txtAlumnado = findViewById(R.id.txtAlumnado);
        btnProfesorado = findViewById(R.id.imgProfesorado);
        txtProfesorado = findViewById(R.id.txtProfesorado);
        txtVersion = findViewById(R.id.txtVersion);

        authProvider = new AuthProvider();
        userProvider = new UserProvider();

        alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Cargando...")
                .setTheme(R.style.Custom)
                .setCancelable(false).build();

        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(MainLogin.this, googleSignInOptions);

        // comprobamos si la sesion sigue abierta
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            // El usuario ha iniciado sesión en Google
            Intent intent = new Intent(MainLogin.this, MainHome.class);
            startActivity(intent);
        } else {
            // El usuario no ha iniciado sesión en Google
            btnProfesorado.setOnClickListener(v -> {
                esProfesorado = true;
                alertDialog.show();
                signIn();
            });

            btnAlumnado.setOnClickListener(v -> {
                esProfesorado = false;
                alertDialog.show();
                signIn();
            });
        }


    }


    // logeo, salta pantalla de google
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        activityResultLaunch.launch(signInIntent);
    }

    ActivityResultLauncher<Intent> activityResultLaunch = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(result.getData());
                        try {
                            // Google Sign In was successful, authenticate with Firebase
                            GoogleSignInAccount account = task.getResult(ApiException.class);
                            Log.d(TAG, "Correcto, entrada en autentificacion con firebase" + account.getId());
                            // comprueba si existe usuario en nuestro firebase
                            firebaseAuthWithGoogle(account.getIdToken());
                        } catch (ApiException e) {
                            // Google Sign In failed, update UI appropriately
                            Log.w(TAG, "Error, no se puedo autentificar con firebase", e);
                        }
                    } else {
                        Log.w(TAG, "Error al logear en google");
                    }
                }
            });

    private void firebaseAuthWithGoogle(String idToken) {

        authProvider.googleLogin(idToken).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                // en el caso de que exista en firebase obtenemos la id del usuario para crear o obtener
                // los datos de la base creada
                id = authProvider.getUid();
                if (esProfesorado) {
                    // averiguamos el tipo de cliente de correo
                    String email = authProvider.getEmail();
                    String[] partes = email.split("@");
                    String vedruna = partes[1];
                    if (vedruna.equals("a.vedrunasevillasj.es") ||vedruna.equals("gmail.com")) {   // cambiar a correo profesor
                        // comprobamos que el rol es correcto
                        userProvider.getUser(id).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                String rol = documentSnapshot.getString("rol");
                                if(rol.equals("a")||rol.equals("b")){
                                    profesoradoExistente(id);
                                }else{
                                    Toast.makeText(MainLogin.this, "El correo no es de profesorado", Toast.LENGTH_SHORT).show();
                                    alertDialog.dismiss();
                                    cerraSesion();
                                }
                            }
                        });

                    } else {
                        cerraSesion();
                        alertDialog.dismiss();
                        Toast.makeText(this, "Introduzca correo del centro.", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    // averiguamos el tipo de cliente de correo
                    String email = authProvider.getEmail();
                    String[] partes = email.split("@");
                    String vedruna = partes[1];
                    if (vedruna.equals("a.vedrunasevillasj.es")||vedruna.equals("gmail.com")) {
                        // comprobamos si existe ya dentro de nuestra base de datos
                        // comprobamos que el rol es correcto
                        userProvider.getUser(id).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                String rol = documentSnapshot.getString("rol");
                                if(rol.equals("c")){
                                    profesoradoExistente(id);
                                }else{
                                    Toast.makeText(MainLogin.this, "El correo no es de alumnado", Toast.LENGTH_SHORT).show();
                                    alertDialog.dismiss();
                                    cerraSesion();
                                }
                            }
                        });
                    } else {
                        cerraSesion();
                        Toast.makeText(this, "Introduzca correo del centro.", Toast.LENGTH_SHORT).show();
                    }
                }
                Log.d(TAG, "Logeo correcto");
            } else {
                // If sign in fails, display a message to the user.
                Log.w(TAG, "Logeo incorrecto", task.getException());
            }
        });
    }

    private void profesoradoExistente(String id) {
        // comprueba si existe con el id obtenido
        userProvider.getUser(id).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    Animacion anim = new Animacion(imgLogo, btnAlumnado, btnProfesorado, txtTitulo, txtVersion, txtAlumnado, txtProfesorado);
                    Intent intent = new Intent(MainLogin.this, MainHome.class);
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainLogin.this, anim.animacion());
                    startActivity(intent, options.toBundle());
                    alertDialog.dismiss();
                } else {
                    Animacion anim = new Animacion(imgLogo, btnAlumnado, btnProfesorado, txtTitulo, txtVersion, txtAlumnado, txtProfesorado);
                    Intent intent = new Intent(MainLogin.this, MainCompletarRegistroUsuario.class);
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainLogin.this, anim.animacion());
                    startActivity(intent, options.toBundle());
                    alertDialog.dismiss();
                }
            }
        });
    }

    public static void cerraSesion() {
        mGoogleSignInClient.signOut();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}