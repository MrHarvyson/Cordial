package com.josec.cordial.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.storage.UploadTask;
import com.josec.cordial.R;
import com.josec.cordial.models.User;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.ImageProvider;
import com.josec.cordial.providers.UserProvider;
import com.josec.cordial.utils.FileUtil;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.io.IOException;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class EditarPerfilActivity extends AppCompatActivity {

    CircleImageView btnFoto;
    ImageView btnFotoPerfil;
    Button btnCambiar;
    boolean bfoto, bimagen = false;
    File imagenFoto, imagenPerfil;
    ImageProvider imageProvider;
    UserProvider userProvider;
    AuthProvider authProvider;
    String nombreUsuario, primerApellido, segundoApellido,imagenesPerfil,nick,fotos;
    EditText etNick;
    User user;
    AlertDialog alertDialog;
    private static final String TAG = "logeron";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        //para ocultar barra con el titulo
        getSupportActionBar().hide();

        btnFoto = findViewById(R.id.imgFoto);
        btnFotoPerfil = findViewById(R.id.imagenPerfil);
        btnCambiar = findViewById(R.id.btnCambiar);
        etNick = findViewById(R.id.etNickCambiar);


        imageProvider = new ImageProvider();
        userProvider = new UserProvider();
        authProvider = new AuthProvider();
        user = new User();

        alertDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Guardando cambios...")
                .setTheme(R.style.Custom)
                .setCancelable(false).build();


        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(EditarPerfilActivity.this)
                        .cropSquare()
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start(101);

                bfoto = true;
            }
        });

        btnFotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.Companion.with(EditarPerfilActivity.this)
                        .cropSquare()
                        .compress(1024)
                        .maxResultSize(1080, 1080)
                        .start(101);

                bimagen = true;
            }
        });

        btnCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.show();
                if (imagenFoto != null && imagenPerfil == null ) {
                    saveI(imagenFoto, true);
                }
                if (imagenPerfil != null && imagenFoto == null ) {
                    saveI(imagenPerfil, false);
                }
                if (imagenFoto != null && imagenPerfil != null) {
                    save();
                }
                if (imagenFoto == null && imagenPerfil == null && etNick != null) {
                    nick = etNick.getText().toString();
                    user(fotos,imagenesPerfil);
                }
                if (imagenFoto == null && imagenPerfil == null && etNick == null) {
                    Toast.makeText(EditarPerfilActivity.this, "No realizó ningún cambio", Toast.LENGTH_SHORT).show();
                }

            }
        });

        // cargamos recursos de firebase
        userProvider.getUser(authProvider.getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    nick = documentSnapshot.getString("nick");
                    nombreUsuario = documentSnapshot.getString("nombre");
                    primerApellido = documentSnapshot.getString("primerApellido");
                    segundoApellido = documentSnapshot.getString("segundoApellido");
                    fotos = documentSnapshot.getString("foto");
                    imagenesPerfil = documentSnapshot.getString("imagenPerfil");
                    if (fotos != null && imagenesPerfil != null) {
                        Picasso.get().load(fotos).into(btnFoto);
                        Picasso.get().load(imagenesPerfil).into(btnFotoPerfil);
                    }

                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && bfoto) {
            Uri path = data.getData();
            btnFoto.setImageURI(path);
            bfoto = false;
            bimagen = false;
            try {
                imagenFoto = FileUtil.from(this, path);
            } catch (IOException e) {
                Log.d(TAG, "Error al pasar url a file");
            }
        }

        if (resultCode == Activity.RESULT_OK && bimagen) {
            Uri path = data.getData();
            btnFotoPerfil.setImageURI(path);
            bfoto = false;
            bimagen = false;
            try {
                imagenPerfil = FileUtil.from(this, path);
            } catch (IOException e) {
                Log.d(TAG, "Error al pasar url a file");
            }
        }
    }

    public void saveI(File imagen, final boolean isFoto) {

        imageProvider.save(EditarPerfilActivity.this, imagen).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {

            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    imageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

                        @Override
                        public void onSuccess(Uri uri) {
                            String url = uri.toString();

                            if (isFoto) {
                                user(url,imagenesPerfil);
                            } else {
                                user(fotos,url);
                            }
                        }
                    });
                }
            }
        });
    }

    public void save() {

        imageProvider.save(EditarPerfilActivity.this, imagenFoto).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskImagenFoto) {
                if (taskImagenFoto.isSuccessful()) {
                    imageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri1) {
                            String url1 = uri1.toString();

                            imageProvider.save(EditarPerfilActivity.this, imagenPerfil).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskImagenPerfil) {
                                    if (taskImagenPerfil.isSuccessful()) {
                                        imageProvider.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri2) {
                                                String url2 = uri2.toString();
                                                user(url1,url2);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });

    }



    private void user(String url1, String url2){
        User user = new User();

        user.setNombre(nombreUsuario);
        user.setPrimerApellido(primerApellido);
        user.setSegundoApellido(segundoApellido);
        user.setFoto(url1);
        user.setImagenPerfil(url2);


        if(!etNick.getText().toString().equals("")){
            user.setNick(etNick.getText().toString());
        }else{
            user.setNick(nick);
        }
        user.setId(authProvider.getUid());
        userProvider.updateUser(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                alertDialog.dismiss();
                if (task.isSuccessful()) {
                    Intent intent = new Intent(EditarPerfilActivity.this, MainHome.class);
                    startActivity(intent);
                    Toast.makeText(EditarPerfilActivity.this, "La informacion se actualizao correctamente", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EditarPerfilActivity.this, "La informacion no se pudo actualizar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}