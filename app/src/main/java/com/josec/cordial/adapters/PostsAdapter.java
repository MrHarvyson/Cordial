package com.josec.cordial.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.josec.cordial.R;
import com.josec.cordial.activities.MainHome;
import com.josec.cordial.activities.PostDetailActivity;
import com.josec.cordial.models.Like;
import com.josec.cordial.models.Post;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.CommentsProvider;
import com.josec.cordial.providers.LikesProvider;
import com.josec.cordial.providers.PostProvider;
import com.josec.cordial.providers.UserProvider;
import com.squareup.picasso.Picasso;
import java.util.Date;
import de.hdodenhof.circleimageview.CircleImageView;

public class PostsAdapter extends FirestoreRecyclerAdapter<Post, PostsAdapter.ViewHolder> {

    Context context;
    UserProvider userProvider;
    PostProvider postProvider;
    CommentsProvider commentsProvider;
    MainHome mainHome;
    LikesProvider likesProvider;
    AuthProvider authProvider;


    public PostsAdapter(FirestoreRecyclerOptions<Post> options, Context context){
        super(options);
        this.context = context;
        userProvider = new UserProvider();
        postProvider = new PostProvider();
        commentsProvider = new CommentsProvider();
        likesProvider = new LikesProvider();
        authProvider = new AuthProvider();

    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Post post) {

        DocumentSnapshot document = getSnapshots().getSnapshot(position);
        String postId = document.getId();


        //obtener DATOS USUARIOS de la publicacion - cabecera
        userProvider.getUser(post.getIdUsuario()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {

            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                String nick = documentSnapshot.getString("nick");
                holder.txtNickPerfil.setText(nick);
                String foto = documentSnapshot.getString("foto");
                Picasso.get().load(foto).into(holder.civFoto);
            }
        });

        // nos muestre la cantidad de comentarios por publicacion
        commentsProvider.getCountCommentByPost(post.getId()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int count = queryDocumentSnapshots.size();
                if(count<=1){
                    holder.txtComentarioCount.setText(String.valueOf(count) + " comentario");
                }else {
                    holder.txtComentarioCount.setText(String.valueOf(count) + " comentarios");
                }

            }
        });

        // obtenemos titulo de la publicación
        holder.txtTituloPostCard.setText(post.getTitulo().toUpperCase());

        // obtenemos descripcion de la publicacion
        holder.txtDescripcionPostCard.setText(post.getDescripcion());

        // obtenemos imagen de la publicacion
        if(post.getImagen1() != null){
            if(!post.getImagen1().isEmpty()){
                Picasso.get().load(post.getImagen1()).into(holder.imagen1PostCard);
            }
        }

        // evento para que al hacer click sobre tarjeta entre en la publacion
        holder.viewHolder.findViewById(R.id.imagen1PostCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PostDetailActivity.class);
                intent.putExtra("idPost",postId);
                context.startActivity(intent);
            }
        });

/*
        // evento opciones

        if(!(post.getIdUsuario()).equals(authProvider.getUid()) ){
            // poner invisible el componente
            holder.ic_opciones.findViewById(R.id.ic_opciones).setVisibility(View.INVISIBLE);
        }


        holder.ic_opciones.findViewById(R.id.ic_opciones).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mostrar dialogAlert opciones
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                LayoutInflater inflater = LayoutInflater.from(context);
                View view = inflater.inflate(R.layout.alert_dialog_opcion,null);

                builder.setView(view);

                final AlertDialog dialog = builder.create();
                dialog.show();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

                Button btnEditarPublicacion = view.findViewById(R.id.btnEditarPublicacion);
                Button btnEliminarPublicacion = view.findViewById(R.id.btnEliminarPublicacion);

                btnEditarPublicacion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });

                btnEliminarPublicacion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postProvider.delete(postId);
                        dialog.dismiss();
                    }
                });
            }
        });

 */


        // evento cuando damos a like¡
        holder.ic_like.findViewById(R.id.ic_like).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // preguntams si existe el like ya
                Like like = new Like();
                like.setIdUser(authProvider.getUid());
                like.setIdPost(postId);
                like.setTimeStamp(new Date().getTime());

                // añade o borra el like
                getCrudLikesByPost(like,holder);

                // muestra la cantidad de like de la publicacion

                getNumberLikesByPost(postId,holder);

            }
        });

        // recargar una vez que iniciamos la apliacion
        getIconLikesByPost(postId, authProvider.getUid(), holder);
        getNumberLikesByPost(postId,holder);
    }


    private void getCrudLikesByPost(Like like, ViewHolder holder){
        likesProvider.getLikeByPostAndUser(like.getIdPost(), authProvider.getUid()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int numberDocuments = queryDocumentSnapshots.size();
                if(numberDocuments > 0){
                    String idLike = queryDocumentSnapshots.getDocuments().get(0).getId();
                    holder.ic_like.setImageResource(R.drawable.ic_favourite_not_fill);
                    likesProvider.delete(idLike);
                }else{
                    holder.ic_like.setImageResource(R.drawable.ic_favourite_fill);
                    likesProvider.create(like);
                }
            }
        });
    }

    // metodo recuperar si un post tiene un like de un user

    private void getIconLikesByPost(String idPost, String idUser, final ViewHolder holder){
        likesProvider.getLikeByPostAndUser(idPost,idUser).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int numberDocuments = queryDocumentSnapshots.size();
                if(numberDocuments > 0){
                    holder.ic_like.setImageResource(R.drawable.ic_favourite_fill);
                }else{
                    holder.ic_like.setImageResource(R.drawable.ic_favourite_not_fill);
                }

            }
        });
    }

    // consulta para recuperar likes de todos los usuarios de un post
    private void getNumberLikesByPost(String idPost, final ViewHolder holder){
        // con addSnap se obtine informacion al momento
        likesProvider.getLikesByPost(idPost).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                int numberDocuments = value.size();
                holder.txtMeGusta.setText(String.valueOf(numberDocuments) + " Me gusta");
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_post,parent,false);
        return new ViewHolder(view);
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtTituloPostCard;
        TextView txtDescripcionPostCard;
        TextView txtComentarioCount;
        TextView txtNickPerfil;
        TextView txtNoComments;
        CircleImageView civFoto;
        ImageView imagen1PostCard;
        ImageView ic_like;
        TextView txtMeGusta;
        View viewHolder;

        public ViewHolder(View view){
            super(view);
            txtTituloPostCard = view.findViewById(R.id.txtTituloPostCard);
            txtDescripcionPostCard = view.findViewById(R.id.txtDescripcionPostCard);
            txtNickPerfil = view.findViewById(R.id.nickPerfil);
            txtComentarioCount = view.findViewById(R.id.txtComentarioCount);
            txtNoComments = view.findViewById(R.id.txtNoComments);
            civFoto = view.findViewById(R.id.imgFotoPubli);
            imagen1PostCard = view.findViewById(R.id.imagen1PostCard);
            ic_like = view.findViewById(R.id.ic_like);
            txtMeGusta = view.findViewById(R.id.txtMeGusta);
            viewHolder = view;
        }

    }

}
