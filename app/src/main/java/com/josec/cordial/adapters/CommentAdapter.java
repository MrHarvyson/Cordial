package com.josec.cordial.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.josec.cordial.R;
import com.josec.cordial.activities.PostDetailActivity;
import com.josec.cordial.models.Comment;
import com.josec.cordial.providers.CommentsProvider;
import com.josec.cordial.providers.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends FirestoreRecyclerAdapter<Comment, CommentAdapter.ViewHolder> {

    Context context;
    UserProvider userProvider;
    LayoutInflater inflater;
    CommentsProvider commentsProvider;
    PostDetailActivity postDetailActivity;
    public CommentAdapter(FirestoreRecyclerOptions<Comment> options, Context context){
        super(options);
        this.context = context;
        userProvider = new UserProvider();
        inflater = LayoutInflater.from(context);
        commentsProvider = new CommentsProvider();
        postDetailActivity = new PostDetailActivity();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Comment comment) {

        DocumentSnapshot document = getSnapshots().getSnapshot(position);
        String commentId = document.getId();
        String idUser = document.getString("idUser");

        holder.txtComment.setText(comment.getComment());
        getUserInfo(idUser,holder);


    }

    private void getUserInfo(String idUser,ViewHolder holder){
        userProvider.getUser(idUser).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    if(documentSnapshot.contains("nick")){
                        String userName = documentSnapshot.getString("nick").toUpperCase();
                        holder.txtUserName.setText(userName);
                    }
                    if(documentSnapshot.contains("foto")){
                        String imageProfile = documentSnapshot.getString("foto");
                        if(imageProfile !=null){
                            if(!imageProfile.isEmpty()){
                                Picasso.get().load(imageProfile).into(holder.imagenComment);
                            }
                        }
                    }
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_comment,parent,false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtUserName;
        TextView txtComment;
        CircleImageView imagenComment;
        View viewHolder;

        public ViewHolder(View view){
            super(view);
            txtUserName = view.findViewById(R.id.txtUserName);
            txtComment = view.findViewById(R.id.txtComment);
            imagenComment = view.findViewById(R.id.imagenComment);
            viewHolder = view;
        }

    }

}
