package com.josec.cordial.models;

public class User {

    private String id;
    private String email;
    private String nick;
    private String nombre;
    private String rol;
    private String primerApellido;
    private String segundoApellido;
    private String foto;
    private String imagenPerfil;

    public User(){

    }

    public User(String id, String email, String nick, String nombre, String rol, String primerApellido, String segundoApellido, String foto, String imagenPerfil) {
        this.id = id;
        this.email = email;
        this.nick = nick;
        this.nombre = nombre;
        this.rol = rol;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.foto = foto;
        this.imagenPerfil = imagenPerfil;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getImagenPerfil() {
        return imagenPerfil;
    }

    public void setImagenPerfil(String imagenPerfil) {
        this.imagenPerfil = imagenPerfil;
    }
}
