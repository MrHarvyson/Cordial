package com.josec.cordial.models;

public class Like {

    private String id;
    private String idPost;
    private String idUser;
    private long timeStamp;

    public Like(){

    }

    public Like(String id, String idPost, String idUser, long timeStamp) {
        this.id = id;
        this.idPost = idPost;
        this.idUser = idUser;
        this.timeStamp = timeStamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPost() {
        return idPost;
    }

    public void setIdPost(String idPost) {
        this.idPost = idPost;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
