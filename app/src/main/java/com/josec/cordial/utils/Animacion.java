package com.josec.cordial.utils;

import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Animacion {

    private ImageView imgTransLogo, imgTransAlumnado, imgTransProfesorado;
    private TextView txtTransTitulo, txtTransVersion, txtTransAlumnado, txtTransProfesorado;

    public Animacion(ImageView imgTransLogo, ImageView imgTransAlumnado, ImageView imgTransProfesorado, TextView txtTransTitulo, TextView txtTransVersion, TextView txtTransAlumnado, TextView txtTransProfesorado) {
        this.imgTransLogo = imgTransLogo;
        this.imgTransAlumnado = imgTransAlumnado;
        this.imgTransProfesorado = imgTransProfesorado;
        this.txtTransTitulo = txtTransTitulo;
        this.txtTransVersion = txtTransVersion;
        this.txtTransAlumnado = txtTransAlumnado;
        this.txtTransProfesorado = txtTransProfesorado;
    }

    public Pair[] animacion() {

        Pair[] pairs = new Pair[7];
        pairs[0] = new Pair<View, String>(this.imgTransLogo, "imgTransLogo");
        pairs[1] = new Pair<View, String>(this.imgTransAlumnado, "imgTransAlumnado");
        pairs[2] = new Pair<View, String>(this.imgTransProfesorado, "imgTransProfesorado");
        pairs[3] = new Pair<View, String>(this.txtTransTitulo, "txtTransTitulo");
        pairs[4] = new Pair<View, String>(this.txtTransVersion, "txtTransVersion");
        pairs[5] = new Pair<View, String>(this.txtTransAlumnado, "txtTransAlumnado");
        pairs[6] = new Pair<View, String>(this.txtTransProfesorado, "txtTransProfesorado");

        return pairs;
    }

}
