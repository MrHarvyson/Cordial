package com.josec.cordial.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.josec.cordial.activities.EditarPerfilActivity;
import com.josec.cordial.R;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.UserProvider;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class PerfilFragment extends Fragment {

    CircleImageView foto;
    ImageView imagenPerfil, btnEditarPerfil;
    TextView tvNick, txtCorreo;
    CoordinatorLayout coordinatorLayout;

    UserProvider userProvider;
    AuthProvider authProvider;
    AlertDialog alertDialog;

    public PerfilFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_perfil, container, false);

        foto = view.findViewById(R.id.imgFoto);
        imagenPerfil = view.findViewById(R.id.imagenPerfil);
        tvNick = view.findViewById(R.id.tvNickPerfil);
        txtCorreo = view.findViewById(R.id.txtCorreoPerfil);
        btnEditarPerfil = view.findViewById(R.id.btnEditarPerfil);
        coordinatorLayout = view.findViewById(R.id.pag);

        userProvider = new UserProvider();
        authProvider = new AuthProvider();
        alertDialog = new SpotsDialog.Builder()
                .setContext(getContext())
                .setMessage("Cargando...")
                .setTheme(R.style.Custom)
                .setCancelable(false).build();
        alertDialog.show();

        coordinatorLayout.setVisibility(view.INVISIBLE);


        btnEditarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EditarPerfilActivity.class);
                startActivity(intent);
            }
        });

        // no se muestran bien cuando solo guardas una foto
        userProvider.getUser(authProvider.getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                alertDialog.dismiss();
                coordinatorLayout.setVisibility(view.VISIBLE);
                if(documentSnapshot.exists()){
                    tvNick.setText(documentSnapshot.getString("nick"));
                    txtCorreo.setText(documentSnapshot.getString("email"));
                    String fotos = documentSnapshot.getString("foto");
                    String imagenesPerfil = documentSnapshot.getString("imagenPerfil");

                    if (fotos != null && imagenesPerfil != null){
                        Picasso.get().load(fotos).into(foto);
                        Picasso.get().load(imagenesPerfil).into(imagenPerfil);
                    }
                }
            }
        });
        // Inflate the layout for this fragment

        return view;
    }


}