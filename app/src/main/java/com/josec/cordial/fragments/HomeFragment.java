package com.josec.cordial.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.josec.cordial.R;
import com.josec.cordial.activities.PostActivity;
import com.josec.cordial.adapters.PostsAdapter;
import com.josec.cordial.models.Post;
import com.josec.cordial.providers.AuthProvider;
import com.josec.cordial.providers.PostProvider;
import com.josec.cordial.providers.UserProvider;
import com.mancj.materialsearchbar.MaterialSearchBar;

import dmax.dialog.SpotsDialog;


public class HomeFragment extends Fragment implements MaterialSearchBar.OnSearchActionListener{

    View view;
    FloatingActionButton btnAnadirPost;
    RecyclerView recyclerView;
    PostProvider postProvider;
    PostsAdapter postsAdapter;
    PostsAdapter postsAdapterSearch;
    UserProvider userProvider;
    AuthProvider authProvider;
    MaterialSearchBar materialSearchBar;


    private void searchByTitle(String title){
        Query query = postProvider.getPostByTitle(title);
        FirestoreRecyclerOptions<Post> options = new FirestoreRecyclerOptions.Builder<Post>().setQuery(query, Post.class).build();
        postsAdapterSearch = new PostsAdapter(options, getContext());
        postsAdapterSearch.notifyDataSetChanged();
        recyclerView.setAdapter(postsAdapterSearch);
        postsAdapterSearch.startListening();
    }


    private void getAllPost(){
        Query query = postProvider.getAll();
        FirestoreRecyclerOptions<Post> options = new FirestoreRecyclerOptions.Builder<Post>().setQuery(query, Post.class).build();
        postsAdapter = new PostsAdapter(options, getContext());
        postsAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(postsAdapter);
        postsAdapter.startListening();
    }
    // para refresqueo datos
    @Override
    public void onStart() {
        super.onStart();
        getAllPost();
    }

    @Override
    public void onStop() {
        super.onStop();
        postsAdapter.stopListening();
        if(postsAdapterSearch!=null){
            postsAdapterSearch.stopListening();
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        btnAnadirPost = view.findViewById(R.id.btnAnadirPost);
        recyclerView = view.findViewById(R.id.recycleViewHome);
        materialSearchBar = view.findViewById(R.id.searchBar);
        postProvider = new PostProvider();
        userProvider = new UserProvider();
        authProvider = new AuthProvider();
        // para que vaya correstamente el search
        materialSearchBar.setOnSearchActionListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);


        //ponerlo horizontal
        //recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
        //recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));

        // ver o ocultar boton ppublicar
       userProvider.getUser(authProvider.getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
           @Override
           public void onSuccess(DocumentSnapshot documentSnapshot) {
               String rol = documentSnapshot.getString("rol");
               if(rol.equals("c")){
                   btnAnadirPost.setVisibility(View.INVISIBLE);
               }
           }
       });

        btnAnadirPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                irAlPost();
            }
        });
        return view;
    }

    private void irAlPost() {
        Intent intent = new Intent(getContext(), PostActivity.class);
        startActivity(intent);
    }

    // metodos de busqueda
    @Override
    public void onSearchStateChanged(boolean enabled) {
        if(!enabled){
            getAllPost();
        }
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {
        searchByTitle(text.toString().toLowerCase());
    }

    @Override
    public void onButtonClicked(int buttonCode) {

    }
}